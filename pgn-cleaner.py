import tkinter
from tkinter import filedialog
import os


class GUI(tkinter.Tk):

    def __init__(self):
        super().__init__()
        self.title("PGN Cleaner")
        self.path_inp = ""
        self.path_out = ""
        self.frames = [tkinter.Frame(self) for _ in range(7)]
        for frame in self.frames:
            frame.pack(side=tkinter.TOP, fill=tkinter.X)
        self.lbl_inp = tkinter.Label(self.frames[0], text="Input File")
        self.btn_inp = tkinter.Button(self.frames[0], text="...", command=self.set_inp_path)
        self.lbl_inp.pack(side=tkinter.LEFT)
        self.btn_inp.pack(side=tkinter.RIGHT, fill=tkinter.X, expand=True)
        texts = ["Remove Comments", "Remove Glyphs", "Remove PGN Tags", "Remove Variations"]
        self.booleans = [tkinter.BooleanVar() for _ in range(len(texts))]
        self.check_lbls = []
        self.check_boxes = []
        for i in range(len(texts)):
            self.check_lbls.append(tkinter.Label(self.frames[i + 1], text=texts[i]))
            self.check_lbls[-1].pack(side=tkinter.LEFT)
            self.check_boxes.append(
                tkinter.Checkbutton(self.frames[i + 1], variable=self.booleans[i], state=tkinter.DISABLED))
            self.check_boxes[-1].pack(side=tkinter.RIGHT)
        self.run_btn = tkinter.Button(self.frames[len(texts)+1], text="Go", command=lambda: main(self.path_inp, self.booleans), state=tkinter.DISABLED)
        self.run_btn.pack(fill=tkinter.X, expand=True)
        self.mainloop()

    def set_inp_path(self):
        self.path_inp = filedialog.askopenfilename(filetypes=[('Portable Game Notation', '*.pgn')])
        if self.path_inp not in ((), ""):
            self.btn_inp.configure(text=os.path.split(self.path_inp)[1])
            for box in self.check_boxes:
                box.configure(state=tkinter.NORMAL)
            self.run_btn.configure(state=tkinter.NORMAL)


def main(path, booleans):
    booleans = [b.get() for b in booleans]

    # format the .pgn file into a list of strings
    with open(path, 'r') as file:
        content = ' '.join([i.rstrip() for i in file if not i[0] == '\n'])
    for s in ['(', ')', '{', '}', '[', ']']:
        content = content.replace(s, ' ' + s + ' ')
    while content != content.replace('  ', ' '):
        content = content.replace('  ', ' ')
    content = content.split(' ')
    if content[0] == '':
        del(content[0])

    # Remove Comments
    if booleans[0]:
        while True:
            try:
                start = content.index('{')
                end = content.index('}')
                del (content[start:end + 1])
            except ValueError:
                break

    # Remove Glyphs
    if booleans[1]:
        content = [i for i in content if i[0] != '$']

    # Remove PGN Tags
    if booleans[2]:
        while True:
            try:
                start = content.index('[')
                end = content.index(']')
                del (content[start:end + 1])
            except ValueError:
                break

    # Define Mainline in case of "--" occurrence
    while 1:
        try:
            i = content.index("--")
            move_number = content[i - 1]
            variant_index = i + 1
            del (content[i - 1:i + 2])
            layer = 0
            while True:
                if content[i] == "(":
                    layer += 1
                elif content[i] == ")":
                    if layer == 0:
                        del (content[i])
                        break
                    else:
                        layer -= 1
                i += 1
            while True:
                if content[i] == '*':
                    break
                if content[i] == "(" and content[i + 1] == move_number:
                    start = i
                    i += 1
                    while True:
                        if content[i] == "(":
                            layer += 1
                        elif content[i] == ")":
                            if layer == 0:
                                end = i
                                break
                            else:
                                layer -= 1
                        i += 1
                    first_part = content[:variant_index]
                    variant = content[start:end + 1]
                    second_part = content[variant_index:start]
                    third_part = content[end + 1:]
                    content = first_part + variant + second_part + third_part
                i += 1
        except ValueError:
            break

    # Remove Variations
    if booleans[3]:
        while True:
            try:
                start = content.index('(')
                i = start + 1
                layer = 0
                while True:
                    if content[i] == "(":
                        layer += 1
                    elif content[i] == ")":
                        if layer == 0:
                            end = i
                            break
                        else:
                            layer -= 1
                    i += 1
                del (content[start:end + 1])
            except ValueError:
                break
        content = [i for i in content if i[-3:] != "..."]

    content = (' '.join(content) + ' ')
    content = content.replace(' 1. ', '\n1. ')
    if not booleans[0]:
        content = content.replace('{ ', '{')
        content = content.replace(' }', '}')
    content = content.replace('( ', '(')
    content = content.replace(' )', ')')
    content = content.replace('[ ', '\n[')
    content = content.replace(' ] ', ']')
    content = content.replace('[Event', '\n[Event')

    print(content)


g = GUI()
