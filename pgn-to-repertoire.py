import requests
import chess


def cloud_eval(game: chess.Board) -> tuple:
    api_url = "https://lichess.org/api/cloud-eval?fen="
    evaluation = requests.get(api_url + game.fen()).json()
    best_move = evaluation['pvs'][0]['cp']
    cp = evaluation['pvs'][0]['moves'][:4]
    return best_move, cp





b = chess.Board()
print(best_move(b))
